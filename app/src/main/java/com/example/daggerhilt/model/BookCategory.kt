package com.example.daggerhilt.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BookCategory(
    val id: Int,
    val title: String,
    val books: List<Book>
) : Parcelable