package com.example.daggerhilt.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Book(
    val id: Int,
    val title: String,
    val imageRes: Int
) : Parcelable