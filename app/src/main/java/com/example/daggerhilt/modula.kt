package com.example.daggerhilt

import com.example.daggerhilt.fragment.recycler.RecyclerViewViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val modules = module {

    viewModel {
        RecyclerViewViewModel()
    }
}
