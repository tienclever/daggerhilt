package com.example.daggerhilt.network

import com.example.daggerhilt.model.User
import com.example.daggerhilt.model.UserItem
import retrofit2.Call
import retrofit2.http.GET

interface APIService {
    @GET("people")
    fun getData(): Call<User>
}