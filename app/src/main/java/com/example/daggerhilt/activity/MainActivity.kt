package com.example.daggerhilt.activity

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.daggerhilt.databinding.LayoutHomeBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: LayoutHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LayoutHomeBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.test.setOnClickListener {
            Toast.makeText(this, "ok",Toast.LENGTH_SHORT).show()
        }
    }
}