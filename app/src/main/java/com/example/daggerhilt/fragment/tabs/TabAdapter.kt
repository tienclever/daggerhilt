package com.example.daggerhilt.fragment.tabs

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.daggerhilt.fragment.car.BookFragment
import com.example.daggerhilt.fragment.game.GameFragment
import com.example.daggerhilt.fragment.music.MusicFragment

class TagAdapter internal constructor(fragmentManager: FragmentManager?) :
    FragmentStatePagerAdapter(fragmentManager!!) {
    override fun getItem(position: Int): Fragment {
        var frag: Fragment? = null
        when (position) {
            0 -> frag = BookFragment()
            1 -> frag = GameFragment()
            2 -> frag = MusicFragment()
        }
        return frag!!
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        var title = ""
        when (position) {
            0 -> title = "Book"
            1 -> title = "Game"
            2 -> title = "Music"
        }
        return title
    }
}