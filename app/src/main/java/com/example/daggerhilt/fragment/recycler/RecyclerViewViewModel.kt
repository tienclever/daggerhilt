package com.example.daggerhilt.fragment.recycler

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.daggerhilt.model.User
import com.example.daggerhilt.network.API
import com.google.gson.Gson
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Response
import kotlin.coroutines.CoroutineContext

class RecyclerViewViewModel : ViewModel(), CoroutineScope{

    var liveDataUser = MutableLiveData<User>()
    var liveDataResponse = MutableLiveData<Boolean>()

    private val job = Job()

    init {

    }

    fun getDataFromApi() {
        viewModelScope.launch(Dispatchers.IO + NonCancellable + CoroutineName("getDataUser")) {
            API.apiService.getData().enqueue(object : retrofit2.Callback<User> {
                override fun onResponse(
                    call: Call<User>,
                    response: Response<User>
                ) {
                    liveDataResponse.postValue(true)
                    if (response.body() != null) {
                        liveDataUser.postValue(response.body())
                        Log.d("response", Gson().toJson(response))
                    }
                }

                override fun onFailure(call: Call<User>, t: Throwable) {
                    liveDataResponse.postValue(true)
                    Log.d("response", t.toString())
                }
            })
        }
    }

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO

    override fun onCleared() {
        job.cancel()
    }
}