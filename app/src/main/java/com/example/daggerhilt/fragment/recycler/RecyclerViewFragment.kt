package com.example.daggerhilt.fragment.recycler

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.daggerhilt.R
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class RecyclerViewFragment : Fragment(){

    private val viewModel: RecyclerViewViewModel by inject()
    private lateinit var adapter: UserAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        requestApi()
    }

    private fun initView() {
        adapter = UserAdapter()
        rv_data.layoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
        rv_data.adapter = adapter
    }

    private fun requestApi() {
        viewModel.getDataFromApi()

        viewModel.liveDataUser.observe(viewLifecycleOwner, Observer {data ->
            adapter.setData(data)
            btn_linear.setOnClickListener {
                adapter.setData(data)
                rv_data.layoutManager = LinearLayoutManager(requireContext())
                rv_data.adapter = adapter
            }

            btn_grid.setOnClickListener {
                adapter.setData(data)
                rv_data.layoutManager = GridLayoutManager(requireContext(), 2)
                rv_data.adapter = adapter
            }

            btn_staggered_grid.setOnClickListener {
                adapter.setData(data)
                rv_data.layoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
                rv_data.adapter = adapter
            }
        })

        viewModel.liveDataResponse.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                progress.visibility = View.GONE
            } else {
                progress.visibility = View.VISIBLE
            }
        })
    }
}