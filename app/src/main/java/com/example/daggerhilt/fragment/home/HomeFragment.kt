package com.example.daggerhilt.fragment.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.daggerhilt.R
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_start_retrofit.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_tabFragment)
        }
        btn_start_recycler_view.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_recyclerViewFragment)
        }
    }
}