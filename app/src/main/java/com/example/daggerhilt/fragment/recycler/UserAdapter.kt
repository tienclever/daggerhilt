package com.example.daggerhilt.fragment.recycler

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.daggerhilt.R
import com.example.daggerhilt.model.User

class UserAdapter(var list: User ?= null) : RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_name = view.findViewById<TextView>(R.id.tv_name)
        val tv_age = view.findViewById<TextView>(R.id.tv_age)
        val tv_hair_color = view.findViewById<TextView>(R.id.tv_hair_color)
        val tv_species = view.findViewById<TextView>(R.id.tv_species)
        val tv_url = view.findViewById<TextView>(R.id.tv_url)
    }

    fun setData(mUser : User){
        list = null
        list = mUser
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val catFact = list!![position]
            holder.tv_name.text = catFact.name
            holder.tv_age.text = catFact.age
            holder.tv_hair_color.text = catFact.hair_color
            holder.tv_species.text = catFact.species
            holder.tv_url.text = catFact.url
    }

    override fun getItemCount(): Int {
        return if (list != null){
            list!!.size
        }else{
            0
        }
    }
}