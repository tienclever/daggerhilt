package com.example.daggerhilt.fragment.car

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.daggerhilt.R
import com.example.daggerhilt.databinding.FragmentBookBinding
import com.example.daggerhilt.model.Book
import com.example.daggerhilt.model.BookCategory

class BookFragment : Fragment() {

    private var _binding: FragmentBookBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBookBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bookCategoryAdapter = BookCategoryAdapter()
        bookCategoryAdapter.submitList(getData())
        binding.outerRecyclerView.adapter = bookCategoryAdapter
    }
}

fun getData(): ArrayList<BookCategory> {
    val bookCategory = arrayListOf<BookCategory>()
    for (a in 1..10) {
        val bookList = arrayListOf<Book>()
        for (b in 1..5) {
            val book = Book(b, "Book Title $b", R.drawable.ic_launcher_background)
            bookList += book
        }

        bookCategory += BookCategory(a, "Category title $a", bookList)
    }

    return bookCategory
}
