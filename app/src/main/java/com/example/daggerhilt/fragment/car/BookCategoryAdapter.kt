package com.example.daggerhilt.fragment.car

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.daggerhilt.databinding.ItemBook2Binding
import com.example.daggerhilt.databinding.ItemCategoryBookBinding
import com.example.daggerhilt.model.Book
import com.example.daggerhilt.model.BookCategory

class BookCategoryAdapter : ListAdapter<BookCategory, CustomViewHolder>(Companion) {
    private val viewPool = RecyclerView.RecycledViewPool()
    /*private val listBookCategory = mutableListOf<BookCategory>()*/

    companion object : DiffUtil.ItemCallback<BookCategory>() {
        //lấy đối tượng so sánh
        override fun areItemsTheSame(oldItem: BookCategory, newItem: BookCategory): Boolean {
            return oldItem === newItem
        }

        //so sánh nội dung
        override fun areContentsTheSame(oldItem: BookCategory, newItem: BookCategory): Boolean {
            return oldItem.id == newItem.id
        }
    }

    /*fun updateListData(data: List<BookCategory>) {
        listBookCategory.addAll(data)
        submitList(data)
    }*/

    override fun getItemViewType(position: Int): Int {
        val type = if (position % 2 == 0) {
            1
        } else {
            2
        }
        return type
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        return if (viewType == 1) {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemCategoryBookBinding.inflate(inflater, parent, false)
            CustomViewHolder(binding)
        } else {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemBook2Binding.inflate(inflater, parent, false)
            CustomViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        if (holder.binding is ItemCategoryBookBinding) {
            val currentBookCategory = getItem(position)
            val itemBinding = holder.binding
            itemBinding.bookCategory = currentBookCategory
            itemBinding.nestedRecyclerView.setRecycledViewPool(viewPool)
            itemBinding.executePendingBindings()
        } else if ((holder.binding is ItemBook2Binding)) {
            val currentBookCategory = getItem(position)
            holder.binding.tvName.text = currentBookCategory.id.toString()
        }
    }
}

@BindingAdapter(value = ["setBooks"])
fun RecyclerView.setBooks(books: List<Book>?) {
    if (books != null) {
        val bookAdapter = BookAdapter()
        bookAdapter.submitList(books)
        adapter = bookAdapter
    }
}